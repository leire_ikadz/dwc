<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>EJ3</title>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript">

      function agregarAjax()
      {
        var euskera = $("#Euskera").val();
        var castellano = $("#Castellano").val();
        var ingles = $("#Ingles").val();
        var parole = $("#Parole").val();
        $.ajax({
          url:   'agregar.php?hitza='+euskera+'&palabra='+castellano+'&word='+ingles+'&parole='+parole,
          type:  'GET',
          beforeSend: function () {
          },
          success: function (respuesta) {
            if(respuesta != "error"){//Compruebo que no me ha llegado un error antes de añadir la fila
              jResp=JSON.parse(respuesta);
              console.log(respuesta);
              insertarFila(jResp);
            }
          }
        });
      }
      var res;
      function insertarFila(jResp){//Funcion para insertar una nueva fila a un tabla
        res = jResp;
        var tds=$("#palabras tr:first td").length;
        var trs=$("#tabla tr").length;
        var nuevaFila="<tr>";
        for(var i=0;i<jResp.length;i++){
            nuevaFila+="<td id='"+jResp[0]+"-"+i+"'>"+jResp[i]+"</td>";
        }
        nuevaFila+="<td><button onclick=borrarAjax('btn"+jResp[0]+"') id='btn" +jResp[0]+"'>Borrar</button></td>";//boton de borrar para la fila.
        nuevaFila+="</tr>";
        $("#palabras").append(nuevaFila);
      }

      //BORRAR FILA
      function borrarAjax($btnId)
      {
        var fila=$btnId.substring(3);//nos quedamos con el num de id.
        $.ajax({
          url:'borrar.php?fila='+fila,
          type:'GET',
          beforeSend: function () {
          },
          success: function (respuesta) {
            console.log(respuesta)
            if(respuesta == "OK"){//si todo ha ido bien vamos a la funcion de borrar fila
              eliminarFila(fila);
            }else{
              alert("Ha habido un error.")
            }
          }
        });
      }
      //Eliminar la fila de la tabla
      function eliminarFila($laFila){
        sFila="#btn"+$laFila;//cogemos la id del btn clicado
        $(sFila).parent().parent().remove();//eliminamos la fila (el padre del btn es el td y hay que eliminar el tr de ese td por eso es parent() x2)
      }


      /***** ACTUALIZAR *******/
      function actualizarAjax($id,$idioma)
      {
        //var fila=$btnId.substring(3);//nos quedamos con el num de id.
        alert($id+"-"+$idioma+"-"+$pn)
        /*
        $.ajax({
          url:'actualizar.php?id='+$id+'&idioma='+$idioma+'&nuevaPalabra='+$pn,
          type:'GET',
          beforeSend: function () {
          },
          success: function (respuesta) {
            console.log(respuesta)
            if(respuesta == "OK"){//si todo ha ido bien vamos a la funcion de borrar fila
              eliminarFila(fila);
            }else{
              alert("Ha habido un error.")
            }
          }
        });*/
      }


      function formUpdate($clickado){
        var id=$clickado.split("-")[0];
        var i=$clickado.split("-")[1];
        var idioma="";
        switch (i){
          case '1':
            idioma="Hitza";
            break;
          case '2':
            idioma="Palabra";
            break;
          case '3':
            idioma="Word";
            break;
          case '4':
            idioma="Parole";
            break;
          default:
            break;
        }
        var form="<form>";
        form+="<input type='text' name='nPalabra'>";
        form+="<button onclick='actualizarAjax('"+id+"','"+idioma+"')'>Cambiar</button></form>";
        //alert(form)
        $("#formularioUpdate").append(form);
      }
    </script>
  </head>
  <body>
    <?php
      $link = @new mysqli("localhost","leire", "leire", "leire");
      if ($link->connect_errno) {
        die('Connect Error: ' . $link->connect_error);
      }

      $sql_select = "SELECT * from diccionario";
      $res = $link->query($sql_select);
    ?>
      <table id="palabras" border="1">
        <tr>
          <td>P</td><td>Hitza</td><td>Palabra</td><td>Word</td><td>Parole</td>
        </tr>
    <?php
      while($fila = mysqli_fetch_array($res, MYSQLI_ASSOC)){
    ?>
      <tr>
        <td id='<?php echo utf8_encode($fila["Id"])?>-0' ><?php echo utf8_encode($fila["Id"]) ?></td>
        <td id='<?php echo utf8_encode($fila["Id"])?>-1' onclick="formUpdate(this.id)"><?php echo utf8_encode($fila["Hitza"]) ?></td>
        <td id='<?php echo utf8_encode($fila["Id"])?>-2' onclick="formUpdate(this.id)"><?php echo utf8_encode($fila["Palabra"]) ?></td>
        <td id='<?php echo utf8_encode($fila["Id"])?>-3' onclick="formUpdate(this.id)"><?php echo utf8_encode($fila["Word"]) ?></td>
        <td id='<?php echo utf8_encode($fila["Id"])?>-4' onclick="formUpdate(this.id)"><?php echo utf8_encode($fila["Parole"]) ?></td>
        <td><button onclick="borrarAjax(this.id)" id='btn<?php echo utf8_encode($fila["Id"]) ?>'>Borrar</button></td>
      </tr>

    <?php
      }
    ?>
  </table>
    <div class="" id="formulario">
      <p>Euskera <input type="text" id="Euskera" value=""></p>
      <p>Catellano <input type="text" id="Castellano" value=""></p>
      <p>Ingles <input type="text" id="Ingles" value=""></p>
      <p>Parole <input type="text" id="Parole" value=""></p>
      <p><button type="button" name="agregar" onclick="agregarAjax()">Agregar</button></p>
    </div>

    <div class="" id="formularioUpdate">
      
    </div>
  </body>
</html>

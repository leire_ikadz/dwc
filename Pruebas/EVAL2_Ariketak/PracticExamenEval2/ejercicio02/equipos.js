var aEquipos=[];

aEquipos[0]=[];
aEquipos[0][0]='Betis';
aEquipos[0][1]='12';
aEquipos[0][2]='El Real Betis Balompié, también conocido como Real Betis o simplemente Betis, es un club de fútbol de la ciudad de Sevilla, fundado en el año 1907. Milita en la Segunda División de España. Sus máximos galardones son un título de Liga y dos Copas del Rey.';
aEquipos[0][3]='http://www.realbetisbalompie.es';

aEquipos[1]=[];
aEquipos[1][0]='Sevilla';
aEquipos[1][1]='10';
aEquipos[1][2]='El Sevilla Fútbol Club es un club de fútbol español organizado como sociedad anónima deportiva. Tiene su sede en Sevilla, capital de la comunidad autónoma de Andalucía, y actualmente juega en Primera División. Se funda el 14 de octubre de 1905, siendo su primer presidente el jerezano José Luis Gallegos Arnosa.El estadio Ramón Sánchez-Pizjuán, propiedad del club, es el escenario que utiliza el equipo para jugar sus partidos oficiales locales. Está situado en el barrio de Nervión y debe su nombre al que fuera su presidente durante diecisiete años. Tiene capacidad para 45500 espectadores.';
aEquipos[1][3]='http://www.sevillafc.es';

aEquipos[2]=[];
aEquipos[2][0]='Espanol';
aEquipos[2][1]='9';
aEquipos[2][2]='El Real Club Deportivo Español (oficialmente, Real Club Deportivo Espanyol de Barcelona S. A. D.) es un club de fútbol español que juega en la Primera División. Fue fundado el 28 de octubre de 1900 por un grupo de universitarios españoles que estaban estudiando en la Universidad de Barcelona y cuyo primer presidente fue Ángel Rodríguez Ruiz. Nació bajo el nombre de Sociedad Española de Football debido a que todos sus componentes eran catalanes o de otros lugares de España, en contraposición con algunos equipos que se componian de varios jugadores extranjeros, como era el caso del Fútbol Club Barcelona.8 9 El R. C. D. Español es además el sexto equipo más antiguo de España.';
aEquipos[2][3]='http://www.rcdespanyol.com';

aEquipos[3]=[];
aEquipos[3][0]='Valencia';
aEquipos[3][1]='18';
aEquipos[3][2]='Valencia Fútbol Club es un club profesional de fútbol venezolano, de la ciudad de Valencia. Fue fundado el 24 de septiembre de 1964 con el nombre Valencia Fútbol Club. Participó en el campeonato de Tercera División de Venezuela, concretamente en el Apertura 2013. En 1964 se fundó el equipo por iniciativa de Oswaldo Michelena, Ecari, Rodolfo Noya, el entrenador brasileño Orlando Fantoni y el periodista Francisco Silvino Nagaris, incorporándose luego Concheto Di Tommasi. Su debut lo hizo directamente en la Primera División de Venezuela en 1965. Valencia FC, debutante, estuvo a punto de no participar en aquel torneo de 1965 y desaparecer, según escribió el periodista Ildemaro Alguíndigue.';
aEquipos[3][3]='http://www.valenciacf.com';

aEquipos[4]=[];
aEquipos[4][0]='At Madrid';
aEquipos[4][1]='15';
aEquipos[4][2]='El Club Atlético de Madrid es un club de fútbol ubicado en la ciudad de Madrid, fundado el 26 de abril de 1903 como Athletic Club de Madrid. Tiene sede en el Estadio Vicente Calderón (Paseo Virgen del Puerto 67) y compite en la Primera División de España Es el cuarto club español con más títulos oficiales de fútbol, habiendo logrado siete títulos internacionales (una Copa intercontinental, una Recopa de Europa, dos Ligas Europeas de la UEFA, dos Supercopas de Europa y una Intertoto) y veinticinco nacionales (diez Ligas, diez Copas del Rey y dos Supercopas de España, junto a tres títulos antecesores de esta última). Tras ganar la Liga 2013/14 se encuentra situado en la cuarta posición de la clasificación histórica de la Primera División Española.4 Actualmente el Atlético es el supercampeón de España, tras derrotar al Real Madrid en la final de la Supercopa 2014.'
aEquipos[4][3]='http://clubatleticodemadrid.com';

aEquipos[5]=[];
aEquipos[5][0]='Malaga';
aEquipos[5][1]='8';
aEquipos[5][2]='El Club Deportivo Málaga fue un equipo de fútbol de español de la ciudad de Málaga. Se le conocía como Club Deportivo Málaga desde 1941. Anteriormente se denominaba Club Deportivo Malacitano, nombre adquirido en 1933 tras la unión de los entonces equipos más representativos de la ciudad: el Fútbol Club Malagueño y el Málaga Sport Club. A su vez eran herederos de dos equipos interiores, el fundacional Málaga Football Club que surgió en 1904 y el efímero Real Málaga FC.'
aEquipos[5][3]='http://www.malagacf.com';

aEquipos[6]=[];
aEquipos[6][0]='Athletic';
aEquipos[6][1]='16';
aEquipos[6][2]='El Athletic Club es un club de fútbol de la villa de Bilbao, País Vasco, España. Fue fundado en 1898 y es, junto al Real Madrid Club de Fútbol y al Fútbol Club Barcelona, el único club que ha disputado todas las ediciones de la Primera División de España desde su creación en 1928. A su vez, es uno de los cuatro únicos clubes profesionales de España que no es una sociedad anónima deportiva, de manera que la propiedad del club recae en sus socios.7 8 Se lo conoce ampliamente como Athletic de Bilbao, y en algunas ocasiones con el nombre de la ciudad, Bilbao, o incluso como Atlético de Bilbao.'
aEquipos[6][3]='http://www.athletic-club.eus';

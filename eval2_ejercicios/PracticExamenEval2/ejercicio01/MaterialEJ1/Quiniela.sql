-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 06-02-2017 a las 22:17:08
-- Versión del servidor: 5.5.44-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `enrique`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Quiniela`
--

CREATE TABLE IF NOT EXISTS `Quiniela` (
  `Jornada` int(11) NOT NULL AUTO_INCREMENT,
  `Partido1` varchar(50) COLLATE utf32_spanish2_ci NOT NULL,
  `GolesCasa1` int(2) NOT NULL,
  `GolesFuera1` int(2) NOT NULL,
  `Partido2` varchar(50) COLLATE utf32_spanish2_ci NOT NULL,
  `GolesCasa2` int(2) NOT NULL,
  `GolesFuera2` int(2) NOT NULL,
  `Partido3` varchar(50) COLLATE utf32_spanish2_ci NOT NULL,
  `GolesCasa3` int(2) NOT NULL,
  `GolesFuera3` int(2) NOT NULL,
  `Partido4` varchar(50) COLLATE utf32_spanish2_ci NOT NULL,
  `GolesCasa4` int(2) NOT NULL,
  `GolesFuera4` int(2) NOT NULL,
  `Partido5` varchar(50) COLLATE utf32_spanish2_ci NOT NULL,
  `GolesCasa5` int(2) NOT NULL,
  `GolesFuera5` int(2) NOT NULL,
  PRIMARY KEY (`Jornada`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf32 COLLATE=utf32_spanish2_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `Quiniela`
--

INSERT INTO `Quiniela` (`Jornada`, `Partido1`, `GolesCasa1`, `GolesFuera1`, `Partido2`, `GolesCasa2`, `GolesFuera2`, `Partido3`, `GolesCasa3`, `GolesFuera3`, `Partido4`, `GolesCasa4`, `GolesFuera4`, `Partido5`, `GolesCasa5`, `GolesFuera5`) VALUES
(1, 'Real Sociedad - Ath Bilbao', 2, 3, 'Barcelona - Real Madrid', 1, 0, 'Las Palmas - Betis', 3, 2, 'Osasuna - Sevilla', 3, 2, 'Zaragoza - Baracaldo', 2, 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

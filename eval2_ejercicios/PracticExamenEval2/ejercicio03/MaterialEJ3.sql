-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 07-02-2019 a las 00:06:14
-- Versión del servidor: 5.7.19-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.26-2+ubuntu16.04.1+deb.sury.org+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `enrique`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aeropuerto`
--

CREATE TABLE IF NOT EXISTS `aeropuerto` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `origen` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `destino` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `salida` time NOT NULL,
  `retraso` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `aeropuerto`
--

INSERT INTO `aeropuerto` (`Id`, `origen`, `destino`, `salida`, `retraso`) VALUES
(2, 'Hondarribia', 'Madrid', '15:00:00', 0),
(3, 'Hondarribia', 'Nueva York', '12:00:00', 0),
(4, 'Hondarribia', 'Barcelona', '16:30:00', 0),
(5, 'Hondarribia', 'Pekin', '17:15:00', 0),
(6, 'Pamplona', 'Madrid', '13:00:00', 0),
(7, 'Pamplona', 'Atenas', '12:15:00', 0),
(8, 'Zaragoza', 'Barcelona', '16:10:00', 0),
(9, 'Zaragoza', 'Las Palmas', '09:25:00', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

$(document).ready(function(){
   
    $(".cont").hide(); 
    $("#contInicio").show();
    $("#linkInicio").css("color","rgb(117,10,10)");
    
  
    $(".enlace").click(function(){
        $(".enlace").css("color","black");
        $(this).css("color","rgb(117,10,10)");
        if(this.id=="linkNovelas"){
            $("#linkAutor").css("color","rgb(117,10,10)");
        }
        $(".cont").hide(); //ocultamos todo
        var seleccionado=this.id;//cogemos la id
        seleccionado=seleccionado.substr(4,seleccionado.lenght);//le quitamos la palabra 'link'
        seleccionado="#cont" + seleccionado;//añadimos la palabra 'cont' y la almoadilla para que sepa que es una id
        $(seleccionado).fadeIn(1500);//mostramos el elemento con esa id

        if($("#divCapitulos").is(":visible")){
            $("#divCapitulos").animate({ 
                width: "toggle" 
            }); 
            $("#linkCapitulos").css("color","black");
        }
        $("#linkCapitulos").css("color","black");
    })

    $("#novelasVolver").click(function(){
        $(".cont").hide(); //ocultamos todo
        $("#contAutor").fadeIn(1500);
        $("#linkAutor").css("color","rgb(117,10,10)");
        $("#linkNovelas").css("color","black")
    });
    
    $("#linkCapitulos").click(function(){
        $("#divCapitulos").animate({ 
            width: "toggle" 
        }); 
        $(".enlace").css("color","black");
        $(this).css("color","rgb(117,10,10)");
        if($("#divCapitulos").is(":hidden")){
            $("#linkCapitulos").css("color","black");
        }
    })
    $("#exit").click(function(){
        $("#divCapitulos").animate({ 
            width: "toggle" 
        }); 
        $("#linkCapitulos").css("color","black");
    })
    
    $(".cap").click(function(){
        $(".enlace").css("color","black");
        $("#linkCapitulos").css("color","rgb(117,10,10)");
        $(".cont").hide(); //ocultamos todo
        var seleccionado=this.id;//cogemos la id
        seleccionado=seleccionado.substr(4,seleccionado.lenght);//le quitamos la palabra 'link'
        seleccionado="#cont" + seleccionado;//añadimos la palabra 'cont' y la almoadilla para que sepa que es una id
        $(seleccionado).fadeIn(1500);//mostramos el elemento con esa id
        $("#divCapitulos").animate({ 
            width: "toggle" 
        }); 
    })
})

